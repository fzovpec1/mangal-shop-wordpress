<?php get_sidebar() ?>
<?php get_header()?>
<?php
global $woocommerce;
$currency = get_woocommerce_currency_symbol();
$price = get_post_meta( get_the_ID(), '_regular_price', true);
$sale = get_post_meta( get_the_ID(), '_sale_price', true);
?>
<div class="sub-navigation" style="background: url('<?php echo get_template_directory_uri().'/img/PIN-UP.png'?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-12 shop_buttons">
                <div class="row">
                    <div class="col">
                        <span class="main-item"><img src="<?php echo get_template_directory_uri().'/img/types/grill1.png'?>" alt=""></span>
                        <span class="hover-item"><img src="<?php echo get_template_directory_uri().'/img/types/grill2.png'?>" alt=""></span>
                        <p>Грили</p>
                    </div>
                    <div class="col">
                        <span class="main-item"><img src="<?php echo get_template_directory_uri().'/img/types/bbq.png'?>" alt=""></span>
                        <span class="hover-item"><img src="<?php echo get_template_directory_uri().'/img/types/bbq2.png'?>" alt=""></span>
                        <p>Барбекю</p>
                    </div>
                    <div class="col">
                        <span class="main-item"><img src="<?php echo get_template_directory_uri().'/img/types/коптильни.png'?>" alt=""></span>
                        <span class="hover-item"><img src="<?php echo get_template_directory_uri().'/img/types/коптильни2.png'?>" alt=""></span>
                        <p>Коптильни</p>
                    </div>
                    <div class="col">
                        <span class="main-item"><img src="<?php echo get_template_directory_uri().'/img/types/мангалы.png'?>" alt=""></span>
                        <span class="hover-item"><img src="<?php echo get_template_directory_uri().'/img/types/мангалы2.png'?>" alt=""></span>
                        <p>Мангалы</p>
                    </div>
                    <div class="col">
                        <span class="main-item"><img src="<?php echo get_template_directory_uri().'/img/types/казаны.png'?>" alt=""></span>
                        <span class="hover-item"><img src="<?php echo get_template_directory_uri().'/img/types/казаны2.png'?>" alt=""></span>
                        <p>Казаны</p>
                    </div>
                    <div class="col">
                        <span class="main-item"><img src="<?php echo get_template_directory_uri().'/img/types/очаги.png'?>" alt=""></span>
                        <span class="hover-item"><img src="<?php echo get_template_directory_uri().'/img/types/очаги2.png'?>" alt=""></span>
                        <p>Очаги</p>
                    </div>
                    <div class="col">
                        <span class="main-item"><img src="<?php echo get_template_directory_uri().'/img/types/ловушки.png'?>" alt=""></span>
                        <span class="hover-item"><img src="<?php echo get_template_directory_uri().'/img/types/ловушки2.png'?>" alt=""></span>
                        <p>Ловушки</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<a href="tel:+7(120)345-67-89"><img class="call" src="img/phone.png" alt=""></a>
<a href="#" onclick="return up()"><img class="up" src="img/top2.png" alt=""></a>
<style>
    .swiper_1 img{
        width: 180px;
        height: 180px;
    }
    .swiper_2 img{
        width: 180px;
        height: 180px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="swiper-container swiper_1">
            <div class="swiper-wrapper">
                <?php
                global $product;
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 12
                    );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) {
                        while ( $loop->have_posts() ) : $loop->the_post();
                            if( $product->is_on_sale() ) {
                                echo '<div class="swiper-slide">';
                                echo '<a href="';
                                echo the_permalink();
                                echo '&quantity=1">';
                                echo woocommerce_get_product_thumbnail();
                                echo '<h4>';
                                echo the_title();
                                echo '</h4>';
                                echo '<p>' . get_post_meta(get_the_ID(), '_sale_price', true) . 'РУБ' . '<s>' . get_post_meta(get_the_ID(), '_regular_price', true) . 'РУБ' . '</s>' . '</p>';
                                echo '</a>';
                                echo '</div>';
                            }
                        endwhile;
                    } else {
                        echo __( 'No products found' );
                    }
                    wp_reset_postdata();
                ?>
            </div>
            <!-- Add Pagination -->
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>

        <div class="col-md-12 buttons">
            <button>перейти в магазин</button>
        </div>
    </div>
    <div class="shop">
        <div class="swiper-container swiper_2">
            <div class="swiper-wrapper">
                <?php
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => -1
                );
                $loop = new WP_Query( $args );
                if ( $loop->have_posts() ) {
                    while ( $loop->have_posts() ) : $loop->the_post();
                        echo '<a href="';
                        echo the_permalink().'&quantity=1';
                        echo '">';
                        echo '<div class="swiper-slide">';
                        echo woocommerce_get_product_thumbnail();
                        echo '<h4>';
                        echo the_title();
                        echo '</h4>';
                        if( $product->is_on_sale() ) {
                            echo '<p>' . get_post_meta(get_the_ID(), '_sale_price', true) . 'РУБ' . '<s style="color: #b42a0a;">' . get_post_meta(get_the_ID(), '_regular_price', true) . 'РУБ' . '</s>' . '</p>';
                        }
                        else{
                            echo '<p>' . get_post_meta(get_the_ID(), '_sale_price', true) . 'РУБ' . '</p>';
                        }
                        echo '</div>';
                        echo '</a>';
                    endwhile;
                } else {
                    echo __( 'No products found' );
                }
                wp_reset_postdata();
                ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev "></div>
        </div>
        <span class="show-more">Показать больше...</span>
    </div>
    <div class="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-md-6 sub-form" style="height: auto;">
                    <form>
                        <h4>Подпишитесь на нашу рассылку</h4>
                        <p>Разнообразный и богатый опыт дальнейшее авыжы плс развитие раз</p>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="введите ваш e-mail адрес">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Подписаться</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12 col-md-1 social-networks">
                    <ul class="col-sm-12">
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/facebook.png'?>" alt=""></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/social_1.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/vk.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/youtube.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/twitter.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/instagram.png'?>"></li>
                    </ul>
                </div>


                <div class="contacts col-sm-6 col-md-3">
                    <span class="col-md-12 col-sm-12"><?php echo get_theme_mod("phone")?></span><br>
                    <span class="col-md-12 col-sm-12"><?php echo get_theme_mod("phone2")?></span><br>
                    <span class="col-md-12 col-sm-12"><?php echo get_theme_mod("email")?></span>
                    <button class="col-sm-12" type="button" name="button">Задать вопрос</button>
                </div>
                <div class="shop-img col-sm-6 col-md-1">
                    <img class="" src="img/shop.png" alt="">
                </div>



            </div>



        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="swiper-container swiper_3">
            <div class="swiper-wrapper">
                <?php
                    while (have_posts() ) : the_post();
                        echo '<div class="swiper-slide" style="background:url('.get_the_post_thumbnail_url().')">';
                        echo '<h1>';
                        echo the_title();
                        echo '</h1><br>';
                        echo '<p>';
                        echo get_the_content();
                        echo '</p>';
                        echo '<a href = "'; echo the_permalink();echo '"><button type="button" name="button">Подробнее</button></a>';
                        echo '</div>';
                    endwhile;
                ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>

        </div>
    </div>
</div>
<div class="container-fluid  blog">
    <div class="container blog-blog">

        <div class="news">
            <div class="row">
                <div class="col-md-7">
                    <img class="col-md-12" height="350px" src="<?php echo get_theme_mod("background_photo")?>" alt="">
                    <h4><?php echo get_theme_mod("f_title")?></h4>
                    <p><?php echo get_theme_mod("f_desc")?></p>
                </div>
                <div class="col-md-5">
                    <img class="col-md-12" height="350px" src="<?php echo get_theme_mod("background_photo1")?>" alt="">
                    <h4><?php echo get_theme_mod("s_title")?></h4>
                    <p><?php echo get_theme_mod("s_desc")?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo get_template_directory_uri().'/js/popup.js'?>"></script>
<script src="<?php echo get_template_directory_uri(). '/js/swiper.min.js'?>"></script>
<script src="<?php echo get_template_directory_uri().'/js/scroll.js'?>"></script>
<script src="<?php echo get_template_directory_uri(). '/js/swiper.js'?>"></script>
<script src="<?php echo get_template_directory_uri().'/js/footer.js'?>"></script>
<?php get_footer() ?>
</body>
</html>

