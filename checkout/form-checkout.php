<!DOCTYPE html>
<html lang="en" style="margin-top: 0 !important;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <?php wp_enqueue_style( 'style', get_stylesheet_uri() ); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <title></title>
    <style>
        @font-face{font-family:BebasNeueBold;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Bold.otf'?>")}
        @font-face{font-family:BebasNeueRegular;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Regular.otf'?>")}
        @font-face{font-family:BebasNeueBook;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Book.otf'?>")}
        .swiper-button-next {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/right.png'?>);
        }
        .swiper-button-prev {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/left.png'?>);
        }
        .cart__body span{
            padding-left: 0;
        }
        .cart__body__img img{
            width: 70%;
        }
    </style>
</head>

<body>
<div class="hide__cross">
    <img src="<?php echo get_template_directory_uri().'/img/cross.png'?>" alt="">
</div>
<div class="hide__cross">
    <img src="<?php echo get_template_directory_uri().'/img/cross.png'?>" alt="">
</div>
<div class="container">
    <div class="row">
        <div style="width: 50vw" class="mobile cart">
            <h2 style="margin-bottom: 7%">Подтверждение заказа</h2>
            <div  style="margin-bottom: 7%">
                <span>Спасибо, Ваш заказ успешно создан!</span><br>
                <span>Наш менеджер скоро свяжется с Вами в ближайшее время.</span>
            </div>
            <?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );?>
            <div style="border:0; padding-left:0" class="access cart__body">
                <div class="container">
                    <div class="row">
                        <span style="border:0; padding-left:0; " class="col-md-3 cart__body__img"><img width="70%" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($product_id)); ?>" alt=""></span>
                        <span style="border:0; padding-left:0;" class="col-md-3">
                                <p><?php
                                    if ( ! $product_permalink ) {
                                        echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                                    } else {
                                        echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                                    }?></p>
                            </span>
                        <span style="border:0; padding-left:0" class="col-md-2">
                                <p>Цвет</p>
                            </span>
                        <span style="border:0; padding-left:0" class="col-md-2">
                                <p>
                                    <?php
                                        echo $cart_item['quantity'];
                                    ?>
                                </p>
                            </span>
                        <span style="border:0; padding-left:0" class="col-md-2">
                                <p>
                                    <?php
								        echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							        ?>
                                </p>
                            </span>
                    </div>
                </div>

            </div>
            <?php } ?>
            <button class="btn_second" class="btn btn-primary">Назад в магазин</button>
        </div>
    </div>
</div>
</body>

</html>