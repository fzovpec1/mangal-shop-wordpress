<!DOCTYPE html>
<html lang="en" style="margin-top: 0 !important;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <?php wp_enqueue_style( 'style', get_stylesheet_uri() ); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <title></title>
    <style>
        @font-face{font-family:BebasNeueBold;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Bold.otf'?>")}
        @font-face{font-family:BebasNeueRegular;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Regular.otf'?>")}
        @font-face{font-family:BebasNeueBook;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Book.otf'?>")}
        .swiper-button-next {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/right.png'?>);
        }
        .swiper-button-prev {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/left.png'?>);
        }
    </style>
</head>

<body>
<header class="header">
    <div class="header__left">
        <div class="container-fluid">
            <div class="row">
                <img src="<?php echo get_template_directory_uri(). '/img/menu%20(1).png'?>" class="header__menu">
                <span class="header__language">EN</span>
                <img src="<?php echo get_template_directory_uri(). '/img/play%20(1).png'?>" class="header__play">
                <img src="<?php echo get_template_directory_uri().'/img/search%20(1).png'?>">
                <form class="hide-search form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search">
                    <input class="btn btn-outline-success my-2 my-sm-0" id="x" type="submit"></input>
                </form>
                <label class="labelx" for="x"><img src="img/search%20(1).png"></label>
            </div>
        </div>
    </div>
    <div class="header__center">
        <span><b>МАНГАЛЫ</b></span>
    </div>
    <div class="header__right">
        <span class="phonenumber header__first"><a href="tel:<?php echo get_theme_mod("phone")?>"><?php echo get_theme_mod("phone")?></a></span>
        <span class="phonenumber"><a href="tel:<?php echo get_theme_mod("phone2")?>"><?php echo get_theme_mod("phone2")?></a></span>
        <img class="social-header" src="<?php echo get_template_directory_uri().'/img/whatsapp%20(1).png'?>" class="header__whats">
        <img class="social-header" src="<?php echo get_template_directory_uri().'/img/skype%20(2).png'?>">
        <a href="<?php echo wc_get_cart_url() ?>"><img src="<?php echo get_template_directory_uri().'/img/корзина.png'?>" class="header__cart" height="27px"></a>
    </div>
    <nav class="header__nav">
        <div class="container">
            <div class="row">
                <ul class="list-group">

                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/home.png'?>"></span>
                            <p class="col-md-6">Главная</p>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/shopping-bag.png'?>"></span>
                            <p class="shop-nav col-md-6">Магазин</p>
                            <ul class="sub__nav">
                                <?php
                                    $orderby = 'name';
                                    $order = 'asc';
                                    $hide_empty = false ;
                                    $cat_args = array(
                                        'orderby'    => $orderby,
                                        'order'      => $order,
                                        'hide_empty' => $hide_empty,
                                    );

                                    $product_categories = get_terms( 'product_cat', $cat_args );

                                    if( !empty($product_categories) ){
                                        foreach ($product_categories as $key => $category) {
                                            $product_id = $category->term_id;
                                            if ($category->name != 'Top' && $category->name != 'Middle') {
                                                echo '<li>';
                                                echo '<a href = "'.get_category_link((int) $product_id).'">';
                                                echo $category->name;
                                                echo '</a>';
                                                echo '</li>';
                                            }

                                        }
                                    }
                                ?>
                            </ul>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/discount.png'?>"></span>
                            <p class="col-md-6">Новости</p>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/chat.png'?>"></span>
                            <p class="col-md-6">Блог</p>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/phone-call.png'?>"></span>
                            <p class="contacts-nav col-md-6">Контакты</p>
                            <ul class="sub__nav__cont">
                                <li><a href="tel:<?php echo get_theme_mod("phone")?>"><?php echo get_theme_mod("phone")?></a></li>
                                <li><a href="tel:<?php echo get_theme_mod("phone2")?>"><?php echo get_theme_mod("phone2")?></a></li>
                                <li><a href="mailto:<?php echo get_theme_mod("email")?>">Почта</a></li>
                                <li>
                                    <div class="row">
                                        <span class="col-md-2"><img height="20px" src="<?php echo get_template_directory_uri().'/img/whatsapp (1).png'?>"></span>
                                        <p class="col-md-6">whatsapp</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <span class="col-md-2"><img height="20px" src="<?php echo get_template_directory_uri().'/img/skype (2).png'?>"></span>
                                        <p class="col-md-6">skype</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/credit-card.png'?>"></span>
                            <p class="col-md-6">Оплата</p>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/deliver.png'?>" alt=""></span>
                            <p class="col-md-6">Доставка</p>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/cart.png'?>" alt=""></span>
                            <p class="col-md-6">Корзина</p>
                        </div>
                    </li>
                    <li class="list-group-item last">
                        <div class="row">
                            <span class="col-md-3"><img src="<?php echo get_template_directory_uri().'/img/recipe-book.png'?>" alt=""></span>
                            <p class="col-md-6">Рецепты</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>