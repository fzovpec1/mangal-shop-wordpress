<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <?php wp_enqueue_style( 'style', get_stylesheet_uri() ); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    [endif]-->
    <?php wp_head(); ?>
    <title></title>
    <style>
        @font-face{font-family:BebasNeueBold;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Bold.otf'?>")}
        @font-face{font-family:BebasNeueRegular;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Regular.otf'?>")}
        @font-face{font-family:BebasNeueBook;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Book.otf'?>")}
        .swiper-button-next {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/right.png'?>);
        }
        .swiper-button-prev {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/left.png'?>);
        }
    </style>
</head>
<body>
<section style="height:100%; width: 100%; background: white; z-index: 9999" class="cart_set">
    <section class="first_cart">
        <div class="hide__cross">
            <a href="/"><img src="<?php echo get_template_directory_uri().'/img/cross.png'?>" alt=""></a>
        </div>
        <div class="container">
            <div class="row">
                <?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );?>
                <div class="mobile cart">
                    <div class="row">


                        <h2>Корзина</h2>
                        <div class="cart__titles">
                            <div class="container">
                                <div class="row">
                                    <span class="col-md-4">Продукт</span>
                                    <span class="col-md-1">Цвет</span>
                                    <span class="col-md-2">Количество</span>
                                    <span class="col-md-2">Стоимость</span>
                                    <span class="col-md-1">Удалить</span>
                                </div>
                            </div>

                        </div>
                        <div style="padding-left:0" class="access cart__body">
                            <div class="container">
                                <div class="row">
                                    <span style="border:0; ; " class="col-md-3 cart__body__img"><?php
                                        $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                                        if ( ! $product_permalink ) {
                                            echo $thumbnail; // PHPCS: XSS ok.
                                        } else {
                                            printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
                                        }
                                        ?></span>
                                    <span style="border:0; padding-left:0;" class="col-md-2">
                                            <p><?php
                                                if ( ! $product_permalink ) {
                                                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                                                } else {
                                                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                                                }?>
                                            </p>
                                        </span>
                                    <span style="border:0; ;" class="col-md-1">
                                            <select class="" name="">
                                                <option style="background: black;" value=""></option>
                                                <option value=""></option>
                                                <option value=""></option>
                                            </select>
                                        </span>
                                    <span style="border:0; padding-left:0" class="col-md-2">
                                            <div style="border:0;" class="buttons">
                                                <form action="">
                                                    <div class="row">

                                                        <div class="col-md-3">
                                                            <button type="submit" style="background: none; width: auto; color: black" class="plus_3">+</button>
                                                        </div>

                                                        <span class="col-md-1 number"> <?php echo $cart_item['quantity'];?>
                                                        </span>

                                                        <div class="col-md-3">
                                                            <button type="submit" style="background: none; width: 25px; color: black" class="minus_3">-</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </span>
                                    <span style="border:0; ; padding-top: 0" class="col-md-2">
                                            <p>
                                                <?php
                                                echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                                ?>
                                            </p>
                                        </span>
                                    <span class="cross col-md-1">
                                        <style>
                                            .woocommerce a.remove{
                                                width: 0;
                                                height: 0;
                                            }
                                        </style>

                                        <?php
                                        // @codingStandardsIgnoreLine
                                        echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                            '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><img src="'.get_template_directory_uri().'/img/cross_2.png" alt=""></a>',
                                            esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                            __( 'Remove this item', 'woocommerce' ),
                                            esc_attr( $product_id ),
                                            esc_attr( $_product->get_sku() )
                                        ), $cart_item_key);
                                        ?></span>
                                    <?php } ?>
                                    </span>

                                </div>
                            </div>

                        </div>

                        <div class="col-md-9"></div>
                        <div class="col-md-3 cart__price">
                            <div class="row">
                                <span class="col-md-6">Итого:</span>
                                <span class="col-md-2"><?php
                                    echo WC()->cart->cart_contents_total;
                                    ?>
                                </span>
                            </div>

                        </div>
                        <div class="col-md-6"></div>
                        <div style="margin-top: 3%" class="buttons-cart col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="/">
                                        <button type="button" value="" class="bck col-md-12" class="col-md-10">Назад в магазин</button>
                                    </form>

                                </div>
                                <div class="col-md-6">
                                    <button type="button" value="" id="hide_first" class="frwrd col-md-12" class="col-md-10">Оформить заказ</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <section class="second_cart">
        <div class="hide__cross">
            <a href="/"><img src="img/cross.png" alt=""></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="cart">
                    <h2 style="top:45%;margin-bottom: 7%">Оплата и доставка</h2>

                    <div class="cart__details">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="point row">
                                        <span class="col-md-2">Сумма доставки</span>
                                        <span class="col-md-3"><?php
                                            echo WC()->cart->cart_contents_total . 'Р';
                                            ?>
                                        </span>
                                    </div>
                                    <div class="point row">
                                        <span class="col-md-3">Регионы:</span>
                                        <span class="col-md-8">
                                                <form action="" method="post">
                                                    <div class="row">
                                                        <input type="radio" name="rus" value="">
                                                        <p class="col-md-2">Россия</p>
                                                        <input type="radio" name="mos" value="">
                                                        <p class="col-md-2">Москва</p>
                                                        <input type="radio" name="spb" value="">
                                                        <p class="col-md-3">Санкт-Петербург</p>
                                                        <input type="radio" name="choice" value="">
                                                        <p class="col-md-2">Ваш выбор</p>
                                                    </div>

                                                </form>
                                            </span>
                                    </div>
                                    <div class="point row">
                                        <span class="col-md-3">Способ доставки:</span>
                                        <span class="col-md-8">
                                                <form action="">
                                                    <div class="row">
                                                        <input type="radio" value="">
                                                        <p class="col-md-3">Курьер (500р)</p>
                                                        <input type="radio" value="">
                                                        <p class="col-md-2">Почта</p>
                                                        <input type="radio" value="">
                                                        <p class="col-md-3">Какие-то варианты</p>
                                                    </div>

                                                </form>
                                            </span>
                                    </div>
                                    <div class="point row">
                                        <span class="col-md-3">Способ оплаты:</span>
                                        <span class="col-md-8">
                                                <form action="">
                                                    <div class="row">
                                                        <input type="radio" value="">
                                                        <p class="col-md-3">Оплата картой</p>
                                                        <input type="radio" value="">
                                                        <p class="col-md-2">PayPal</p>
                                                        <input type="radio" value="">
                                                        <p class="col-md-3">Оплата курьеру</p>
                                                    </div>

                                                </form>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-5 cost">
                        <div class="row">
                            <span class="col-md-6">Общая стоимость:</span>
                            <span class="price col-md-3"><?php
                                echo WC()->cart->cart_contents_total . 'Р';
                                ?></span>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-6"></div>
                        <div style="margin-top: 3%; float: right" class="buttons-cart col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="/">
                                        <button type="button" value="" class="bck col-md-12" class="col-md-10">Назад в магазин</button>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" value="" id="hide_second" class="frwrd col-md-12" class="col-md-10">Оформить заказ</button>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </section>
    <section class="third_cart">
        <div class="hide__cross">
            <a href="/"><img src="img/cross.png" alt=""></a>
        </div>
        <div class="container">
            <div class="row">
                <div style="width: 65vw" class="mobile cart">
                    <h2 style="margin-bottom: 7%">Оплата и доставка</h2>
                    <form method = 'POST'>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input required type="text" class="form-control" placeholder="Имя" name = 'name'>
                            </div>
                            <div class="form-group col-md-6">
                                <input required type="text" class="form-control" placeholder="Фамилия" name = 'surname'>
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control" name = 'country'>
                                    <option required selected>Страна</option>
                                    <?php
                                    $delivery_zones = WC_Shipping_Zones::get_zones();
                                    foreach ((array) $delivery_zones as $key => $the_zone ) {
                                        echo '<option>'.$the_zone['formatted_zone_location'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <select required class="form-control" name="city">
                                    <option selected>Город</option>
                                    <?php
                                    $delivery_zones = WC_Shipping_Zones::get_zones();
                                    foreach ((array) $delivery_zones as $key => $the_zone ) {
                                        echo '<option>'.$the_zone['zone_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="number" class="form-control" required placeholder="Индекс" name = "index">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" required placeholder="Улица" name = "street">
                            </div>
                            <div class="form-group col-md-2">
                                <input type="number" class="form-control" required placeholder="Дом" name = "house">
                            </div>
                            <div class="form-group col-md-2">
                                <input type="number" class="form-control" required placeholder="Квартира" name = "flat">
                            </div>
                            <div class="form-group col-md-6">
                                <input id="phone_form" type="tel" class="form-control" required placeholder="+7 (999) 999-99-99" name = "phone">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" required placeholder="Ваш Email" name = "email">
                            </div>
                            <textarea rows="8" class="col-md-12" placeholder="Комментарии к заказу" name = "comment"></textarea>
                        </div>
                        <input class="btn_first" type="submit" class="btn btn-primary" name = "send">
                        <button class="btn_second btn btn-primary">Назад в магазин</button>
                    </form>

                </div>


            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#phone_form').mask("+7 (999) 999-99-99");
            })
        </script>
    </section>
</section>
<script src="<?php echo get_template_directory_uri().'/js/popup.js'?>"></script>
</body>

</html>
