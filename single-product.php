<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>


<?php while ( have_posts() ) : the_post(); ?>
<?php
    $quantity = $_GET['quantity'];
    $quantity_plus = $quantity + 1;
    $quantity_minus = $quantity - 1;
?>
<?php endwhile; // end of the loop. ?>
<!DOCTYPE html>
<html lang="en" style="margin-top: 0 !important;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <?php wp_enqueue_style( 'style', get_stylesheet_uri() ); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <title></title>
    <style>
        @font-face{font-family:BebasNeueBold;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Bold.otf'?>")}
        @font-face{font-family:BebasNeueRegular;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Regular.otf'?>")}
        @font-face{font-family:BebasNeueBook;src:url("<?php echo get_template_directory_uri().'/fonts/BebasNeue/BebasNeue Book.otf'?>")}
        .swiper-button-next {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/right.png'?>);
        }
        .swiper-button-prev {
            width: 5vw;
            background-image: url(<?php echo get_template_directory_uri().'/img/left.png'?>);
        }
    </style>
</head>

<body>
    <a href="/"><img src="<?php echo get_template_directory_uri().'/img/крестик%20копия%202.png'?>" style="float: right; margin-top: 20px; margin-right: 60px"></a>
	<div class="container" style="margin-top: 30px">
		<div class="row commodity">
			<div class="col-md-6 commodity__photos">
				<div class="com">
					<div class="swiper-container swiper_4 gallery-top">
                        <div class="swiper-wrapper">
                            <?php
                            /**
                             * Single Product Thumbnails
                             *
                             * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
                             *
                             * HOWEVER, on occasion WooCommerce will need to update template files and you
                             * (the theme developer) will need to copy the new files to your theme to
                             * maintain compatibility. We try to do this as little as possible, but it does
                             * happen. When this occurs the version of the template file will be bumped and
                             * the readme will list any important changes.
                             *
                             * @see         https://docs.woocommerce.com/document/template-structure/
                             * @package     WooCommerce/Templates
                             * @version     3.5.1
                             */

                            defined( 'ABSPATH' ) || exit;

                            // Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
                            if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
                                return;
                            }

                            global $product;

                            $attachment_ids = $product->get_gallery_image_ids();

                            if ( $attachment_ids && $product->get_image_id() ) {
                                foreach ( $attachment_ids as $attachment_id ) {
                                    echo '<div class="swiper-slide" style="background-image:url('.wp_get_attachment_url( $attachment_id ).')"></div>';
                                }
                            }?>
						</div>
                    </div>
						<!-- Add Arrows -->
						<div class="swiper-button-next swiper-button-white"></div>
						<div class="swiper-button-prev swiper-button-white"></div>
					</div>
					<div class="swiper-container gallery-thumbs">
						<div class="swiper-wrapper">
                            <?php
                            /**
                             * Single Product Thumbnails
                             *
                             * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
                             *
                             * HOWEVER, on occasion WooCommerce will need to update template files and you
                             * (the theme developer) will need to copy the new files to your theme to
                             * maintain compatibility. We try to do this as little as possible, but it does
                             * happen. When this occurs the version of the template file will be bumped and
                             * the readme will list any important changes.
                             *
                             * @see         https://docs.woocommerce.com/document/template-structure/
                             * @package     WooCommerce/Templates
                             * @version     3.5.1
                             */

                            defined( 'ABSPATH' ) || exit;

                            // Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
                            if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
                                return;
                            }

                            global $product;

                            $attachment_ids = $product->get_gallery_image_ids();

                            if ( $attachment_ids && $product->get_image_id() ) {
                                foreach ( $attachment_ids as $attachment_id ) {
                                    echo '<div class="swiper-slide" style="background-image:url('.wp_get_attachment_url( $attachment_id ).')"></div>';
                                }
                            }?>
						</div>
					</div>
				</div>

			<div class="col-md-6 commodity__description">
				<span class="commodity__title"><b><?php the_title() ?></b></span>
				<img src="<?php echo get_template_directory_uri().'/img/heart.png'?>"><br>
				<span class="commodity__price"><?php echo get_post_meta( get_the_ID(), '_regular_price', true)?> РУБ</span>
				<div class="commodity__info">
					<span class="commodity__subtitle"><b>ОПИСАНИЕ</b></span>
					<div class="commodity__addition">
                        <?php
                        if ( have_posts() ) : while ( have_posts() ) : the_post();
                            the_content();
                        endwhile;
                        endif;
                        wp_reset_postdata();
                        ?>
					</div>
					<span class="commodity__subtitle"><b>ХАРАКТЕРИСТКИ</b></span>
					<div class="commodity__addition">
                        <?php
                        global $product;
                        $characteristics = $product->get_attribute( 'Characteristics' );
                        echo $characteristics
                        ?>
					</div>
					<div class="commodity__addition" style="color: black">
						<div class="row">
							<div class="col-md-6">
								<span class="commodity__subtitle" style="margin-bottom: 10px"><b>ВЫБЕРИТЕ ЦВЕТ</b></span><br>
								<img src="img/Прямоугольник%205%20копия.png">
								<img src="img/Прямоугольник%205%20копия.png">
								<img src="img/Прямоугольник%205%20копия.png">
							</div>
							<div class="col-md-6">
								<span class="commodity__subtitle" style="margin-bottom: 10px"><b>КОЛИЧЕСТВО</b></span><br>
                                <form method="POST" id = 'p-m' action="#">
                                    <div class="row" style="width: 170px">
                                        <div class="col-md-4">
                                            <span class="commodity__plus"><b><a href ='<?php echo get_permalink( get_the_ID() ).'&quantity='.$quantity_plus; ?>'>+</a></b></span>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="commodity__number"><b><?php echo $quantity;?></b></span>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="commodity__minus"><b><a href ='<?php echo get_permalink( get_the_ID() ).'&quantity='.$quantity_minus; ?>'>-</a></b></span>
                                        </div>
                                    </div>
                                </form>
                                <script>
                                    $("#p-m").submit(function(e) {
                                        e.preventDefault();
                                    });
                                </script>
							</div>
						</div>
					</div>
				</div>
				<div class="commodity__buy">
                    <?php
                    global $product;

                    echo apply_filters( 'woocommerce_loop_add_to_cart_link',
                        sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button %s product_type_%s"><input type = "submit" value="ДОБАВИТЬ В КОРЗИНУ" class="add_cart"></a>',
                            esc_url( $product->add_to_cart_url().'&quantity='.$quantity ),
                            esc_attr( $product->id ),
                            esc_attr( $product->get_sku() ),
                            $product->is_purchasable() ? 'add_to_cart_button' : '',
                            esc_attr( $product->product_type ),
                            esc_html( $product->add_to_cart_text() )
                        ),
                        $product );?>
					<input type = "submit" value="КУПИТЬ" class="buy">
				</div>
			</div>
		</div>
	</div>

	<script src="<?php echo get_template_directory_uri().'/js/swiper.min.js'?>"></script>

	<script src="<?php echo get_template_directory_uri().'/js/swiper.js'?>"></script>

	</body>

	</html>
