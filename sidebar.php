<div class="background">

</div>
<section class="form_something free-call">
    <div class="form_something__cross">
        <img src="img/крестик%20копия.png" width="20">
    </div>
    <h2 class="free-call__title"><b>ЗАПОЛНИТЕ ФОРМУ ДЛЯ ЧЕГО-ТО</b></h2>
    <input placeholder="ВАШЕ ИМЯ"><br>
    <input id="phone" placeholder="+7(999) 999-99-99"><br>
    <input placeholder="УДОБНОЕ ВРЕМЯ ДЛЯ ЗВОНКА"><br>
    <input type="submit" value="ОСТАВИТЬ ЗАЯВКУ" class="free-call__submit" style="">
</section>
<script>
    $(document).ready(function () {
        $('#phone').mask("+7 (999) 999-99-99");
    })
</script>
<section class="daily_product">
    <div class="promotion__cross daily_product__cross">
        <img src="img/крестик%20копия%202.png">
    </div>
    <h2>ТОВАР ДНЯ</h2>
    <div class="daily_product__image">
        <img class="col-xs-12" src="img/daily.png">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3><b>PRODUCT NAME</b></h3>
                <span class="daily_product__price">700.000 РУБ</span>
            </div><br>
            <div class="col-md-6">
                <input type="submit" value="Подробнее" class="welcome__submit">
            </div>
        </div>
    </div>
</section>
<section class="promotion">
    <div class="promotion__cross hide__cross">
        <img src="img/крестик%20копия%202.png">
    </div>
    <h2 class="promotion__title">СКИДКИ ДО 50%</h2>
    <div class="promotion__description"><b>ПРИ ПОКУПКЕ ЧЕГО-ТО ТАМ РАЗНОБРАЗНЫЙ И БОГАТЫЙ ОПЫТ</b></div>
    <div class="container">
        <div class="row promotion__products">
            <div class="col-md-12 col-xs-12 promotion__product show">
                <img src="img/product_1.png" alt="">
                <h4>Product Name</h4>
                <p>1000 РУБ<s>15000 РУБ</s></p>
            </div>
            <div class="col-md-4 promotion__product hide">
                <img src="img/product_1.png" alt="">
                <h4>Product Name</h4>
                <p>1000 РУБ<s>15000 РУБ</s></p>
            </div>
            <div class="col-md-4 promotion__product hide">
                <img src="img/product_1.png" alt="">
                <h4>Product Name</h4>
                <p>1000 РУБ<s>15000 РУБ</s></p>
            </div>
            <div class="col-md-4 promotion__product hide">
                <img src="img/product_1.png" alt="">
                <h4>Product Name</h4>
                <p>1000 РУБ<s>15000 РУБ</s></p>
            </div>
        </div>
    </div>
</section>