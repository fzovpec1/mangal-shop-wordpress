<footer>

    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <ul>
                            <li>
                                <p>Продукты<p>
                            </li>
                            <li>Мангалы</li>
                            <li>Мангалы из металла</li>
                            <li>Грили</li>
                            <li>Казаны</li>
                            <li>Барбекю</li>
                            <li>Очаги</li>
                        </ul>
                        <ul>
                            <li>
                                <p>Инфо<p>
                            </li>
                            <li>Главная</li>
                            <li>Магазин</li>
                            <li>Блог</li>
                            <li>Новости</li>
                        </ul>
                        <ul>
                            <li>
                                <p>Контакты<p>
                            </li>
                            <li><a href="tel:<?php echo get_theme_mod("phone")?>"></a></li>
                            <li><a href="tel:<?php echo get_theme_mod("phone2")?>"></a></li>
                            <li><a href="mailto:<?php echo get_theme_mod("email")?>"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2 col-md-1 social-networks">
                    <ul class="col-sm-12">
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/facebook_white1.png'?>" alt=""></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/odnoklassniki_white1.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/vk_white1.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/youtube_white1.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/twitter_white1.png'?>"></li>
                        <li class="col-sm-2"><img src="<?php echo get_template_directory_uri().'/img/instagram_white1.png'?>"></li>
                    </ul>
                </div>

                <iframe class="col-md-5 col-sm-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.360411981455!2d37.66901575134605!3d55.7522410994075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54ba6a2dbea5d%3A0x6b8d20281306254a!2z0KbQtdC90YLRgCDQtNC40LfQsNC50L3QsCBBUlRQTEFZ!5e0!3m2!1sru!2sru!4v1555596294846!5m2!1sru!2sru"
                        frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>

    </div>

    <div class="mini">
        <div class="container">
            <div class="row">
                <ul class="col-sm-12 col-md-8 menu">
                    <li class="col-sm-12">
                        <p>Продукты</p>
                    </li>
                    <li class="col-sm-12">
                        <p>Инфо</p>
                    </li><br>
                    <li class="col-sm-12"><a href="tel:<?php echo get_theme_mod("phone")?>"><?php echo get_theme_mod("phone")?></a></li><br>
                    <li class="col-sm-12"><a href="tel:<?php echo get_theme_mod("phone2")?>"><?php echo get_theme_mod("phone2")?></a></li><br>
                    <li class="col-sm-12"><a href="mailto:<?php echo get_theme_mod("email")?>"><?php echo get_theme_mod("email")?></a></li>
                </ul>
                <div class="col-sm-2 col-md-4 social-networks">
                    <ul class="col-md-12">
                        <li class="social_img col-md-1"><img src="<?php echo get_template_directory_uri().'/img/facebook_white1.png'?>" alt=""></li>
                        <li class="social_img col-md-1"><img src="<?php echo get_template_directory_uri().'/img/odnoklassniki_white1.png'?>"></li>
                        <li class="social_img col-md-1"><img src="<?php echo get_template_directory_uri().'/img/vk_white1.png'?>"></li>
                        <li class="social_img col-md-1"><img src="<?php echo get_template_directory_uri().'/img/youtube_white1.png'?>"></li>
                        <li class="social_img col-md-1"><img src="<?php echo get_template_directory_uri().'/img/twitter_white1.png'?>"></li>
                        <li class="social_img col-md-1"><img src="<?php echo get_template_directory_uri().'/img/instagram_white1.png'?>"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>