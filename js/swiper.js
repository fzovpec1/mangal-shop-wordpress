var swiper = new Swiper('.swiper_1', {
    slidesPerView: 4,
    spaceBetween: 0,
    slidesPerGroup: 4,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        1024:{
            slidesPerView: 3,
            slidesPerColumn: 1
        },
        800:{
            slidesPerView: 2,
            slidesPerColumn: 1
        },
        640: {
            slidesPerView: 1,
            slidesPerGroup: 1,
        }
    }
});
var swiper__2 = new Swiper('.swiper_2', {
    slidesPerView: 4,
    slidesPerColumn: 3,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        // when window width is <= 320px
        950:{
            slidesPerView: 3,
            slidesPerColumn: 1
        },
        800:{
            slidesPerView: 2,
            slidesPerColumn: 1
        },
        640: {
            slidesPerView: 1,
            slidesPerColumn: 1
        }
    }
});
var swiper__3 = new Swiper('.swiper_3', {
    slidesPerView: 1,
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },

});
var swiper_4 = new Swiper('.swiper_4', {
    slidesPerView: 3,
    spaceBetween: 5,
    slidesPerGroup: 3,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 3,
    loop: true,
    freeMode: true,
    loopedSlides: 3, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});
var galleryTop = new Swiper('.com .gallery-top', {
    spaceBetween: 10,
    loop: true,
    loopedSlides: 3, //looped slides should be the same
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
});
