<?php
/**
 * Created by PhpStorm.
 * User: gleb1
 * Date: 22/04/2019
 * Time: 10:53
 */
add_action('customize_register', function($customizer) {
    /* Настройки сайта */
    $customizer->add_section(
        'down', array(
            'title' => 'Внизу',
            'description' => '',
            'priority' => 11,
        )
    );
    $customizer->add_setting('f_title',
        array('default' => '')
    );

    $customizer->add_control('f_title', array(
            'label' => 'Первый заголовок',
            'section' => 'down',
            'type' => 'text',
        )
    );
    $customizer->add_setting('f_desc',
        array('default' => '')
    );

    $customizer->add_control('f_desc', array(
            'label' => 'Первое описание',
            'section' => 'down',
            'type' => 'textarea',
        )
    );
    $customizer->add_setting('background_photo',
        array('default' => '')
    );
    $customizer->add_control(
        new WP_Customize_Image_Control(
            $customizer,
            'background_photo',
            array(
                'label'      => __( 'Первое изображение','img' ),
                'section'    => 'down',
                'settings'   => 'background_photo',
            )
        )
    );
    $customizer->add_setting('s_title',
        array('default' => '')
    );

    $customizer->add_control('s_title', array(
            'label' => 'Второй заголовок',
            'section' => 'down',
            'type' => 'text',
        )
    );
    $customizer->add_setting('s_desc',
        array('default' => '')
    );

    $customizer->add_control('s_desc', array(
            'label' => 'Второе описание',
            'section' => 'down',
            'type' => 'textarea',
        )
    );
    $customizer->add_setting('background_photo1',
        array('default' => '')
    );
    $customizer->add_control(
        new WP_Customize_Image_Control(
            $customizer,
            'background_photo1',
            array(
                'label'      => __( 'Второе изображение', 'img2' ),
                'section'    => 'down',
                'settings'   => 'background_photo',
            )
        )
    );
    $customizer->add_section(
        'contacts', array(
            'title' => 'Контакты',
            'description' => '',
            'priority' => 8,
        )
    );
    $customizer->add_setting('phone',
        array('default' => '')
    );

    $customizer->add_control('phone', array(
            'label' => 'Первый телефон',
            'section' => 'contacts',
            'type' => 'text',
        )
    );
    $customizer->add_setting('phone2',
        array('default' => '')
    );

    $customizer->add_control('phone2', array(
            'label' => 'Второй телефон',
            'section' => 'contacts',
            'type' => 'text',
        )
    );
    $customizer->add_setting('email',
        array('default' => '')
    );

    $customizer->add_control('email', array(
            'label' => 'Email',
            'section' => 'contacts',
            'type' => 'email',
        )
    );
});
function add_to_cart_redirect(){
    $country = '';
    if(isset($_POST['rus'])){
        $country = 'Россия';
    }
    if(isset($_POST['mos'])){
        $country = 'Москва';
    }
    if(isset($_POST['spb'])){
        $country = 'Санкт-Петербург';
    }
    if(isset($_POST['choice'])){
        $country = 'Другой';
    }
    if(isset($_POST['name'])){
        global $woocommerce, $product, $post;
        $address = array(
            'first_name' => $_POST['name'],
            'last_name'  => $_POST['surname'],
            'email'      => $_POST['email'],
            'phone'      => $_POST['phone'],
            'country'    => $_POST['country'],
            'city'       => $_POST['city'],
            'address_1'  => $_POST['index'].' '.$_POST['street'].' '.$_POST['house'].' '.$_POST['flat'].' '.$country,
            'comment'    => $_POST['comment']
        );

        // Create the order object.
        $order = wc_create_order();

        foreach( WC()->cart->get_cart() as $cart_item ){
            $product_id = $cart_item['product_id'];
            $order->add_product(get_product(''.$product_id.''));
        }
        // Add a product, make sure that this product exists!
        $order->set_address( $address, 'billing' );
        $order->calculate_totals();
        wc_empty_cart();
    }

}
add_filter ('woocommerce_add_to_cart_redirect', 'add_to_cart_redirect');
?>